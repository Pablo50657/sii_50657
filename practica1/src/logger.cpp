/******CREADO POR PABLO CARRILLO CARRASCO******/

/******LOGGER******/

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define FIFO_DIR "/tmp/myfifo"
#define SIZE 49

int main(){

int fd_fifo;
char buffer[SIZE];
if(mkfifo(FIFO_DIR, 0777) < 0){
	perror("error al crear fifo");
	return 1;	
}
printf("fifo creado correctamente\n");

fd_fifo = open(FIFO_DIR, O_RDONLY);//esta accion bloquea el proceso hata que se habra por el otro lado, si no esta fijado O_NONBLOCK
if(fd_fifo < 0){
	perror("error al abrir el fifo");
	return 2;
}
printf("fifo abierto correctamente y leyendo\n");
while(read(fd_fifo,buffer,SIZE)){
	printf("%s\n",buffer);
}

close(fd_fifo);
printf("fifo cerrado correctamente\n");

unlink(FIFO_DIR);
printf("fifo unlinked correctamente\n");

return 0;
}

