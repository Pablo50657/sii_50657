#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include "../include/DatosMemCompartida.h"
#define FILE_DIR "/tmp/myfile"


int main(){
	int fd;
	DatosMemCompartida *pMem;
	fd = open(FILE_DIR,O_RDWR|O_CREAT|O_TRUNC,0666);
	ftruncate(fd,sizeof(DatosMemCompartida));//forzamos el fichero a tener el tamaño de la variable
	pMem = (DatosMemCompartida*)mmap(NULL,sizeof(DatosMemCompartida),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0);
	if (pMem == MAP_FAILED){
	        perror ("error mmap failed");
	}
	close(fd);
	while(1){
		if(((pMem->raqueta1.y1+pMem->raqueta1.y2)/2) > pMem->esfera.centro.y){
		pMem->accion = -1; //arriba
		}
		else if(((pMem->raqueta1.y1+pMem->raqueta1.y2)/2) < pMem->esfera.centro.y){
		pMem->accion = 1; //abajo
		}
		else{
		pMem->accion = 0;//nada
		}
		usleep(25000);
	}
	munmap(pMem,sizeof(DatosMemCompartida));	

return 0;
}
