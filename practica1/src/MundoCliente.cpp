/******EDITADO POR PABLO CARRILLO CARRASCO******/

// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "../include/MundoCliente.h"
#include "../include/glut.h"
//#include "../include/Socket.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	munmap(pMem,sizeof(Mem));//desproyeccion del fichero en memoria
	/*close(fd_fifo_data);
	close(fd_fifo_comm);
	unlink(FIFO_DATA);
	unlink(FIFO_COMM);*/
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);

}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);

	//Bot data
	pMem->raqueta1 = jugador1;
	pMem->esfera = esfera;
	//Bot action
	switch(pMem->accion){
		case 1://arriba
			OnKeyboardDown('w',0,0);
			break;
		case -1://abajo
			OnKeyboardDown('s',0,0);
			break;
		default:
			break;
	}
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.radio = 0.5f;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}
	if(fondo_dcho.Rebota(esfera))
	{
		esfera.radio = 0.5f;		
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}
	//Socket
	s_server.Receive(buffer_recv,100);
	sscanf(buffer_recv,"%f %f %f %f %f %f %f %f %f %f", &esfera.centro.x,&esfera.centro.y, 									&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, 									&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2);
	
	//Se acaba la partida si algun jugador alcanza los 3 puntos
	if((puntos1>=3) || (puntos2>=3)){
		exit(1);
	}
	/*//Recepcion de datos del servidor
	if(read(fd_fifo_data,buffer_data,400)<0){
		perror("error lectura desde servidor");
	}
	sscanf(buffer_data,"%f %f %f %f %f %f %f %f %f %f", &esfera.centro.x,&esfera.centro.y, 									&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, 									&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2);
	*/
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;	
	}
	s_server.Send((char*)key,1);
	//sprintf(buffer_comm,"%c",key);
	//write(fd_fifo_comm,buffer_comm,1);
}

void CMundo::Init()
{
//Comunicacion por socket con servidor
	s_server.Connect(IPDIR,7);
	s_server.Send("patatasa",8);
//Proyeccion en memoria para bot
	fd_mem = open(FILE_DIR,O_RDWR|O_CREAT|O_TRUNC,0666);//necesario un fichero
	ftruncate(fd_mem,sizeof(Mem));
	pMem = (DatosMemCompartida*)mmap(NULL,sizeof(Mem),PROT_WRITE|PROT_READ,MAP_SHARED,fd_mem,0);//el puntero de datosMem
												//contiene la region de memoria compartida
	if (pMem == MAP_FAILED){
	        perror ("error mmap failed");
	}
	close(fd_mem);//cerramos fichero

/*//Crear fifo para comunicacion con servidor
	//datos a recibir
	if(mkfifo(FIFO_DATA,0777) < 0){
		//unlink(FIFO_DATA);
		perror("error al crear fifo de datos para servidor, recreando");
		//mkfifo(FIFO_DATA,0777);	
	}
	fd_fifo_data = open(FIFO_DATA,O_RDONLY);

	//comandos a enviar
	if(mkfifo(FIFO_COMM,0777) < 0){
		//unlink(FIFO_COMM);
		perror("error al crear fifo de comandos para servidor, recreando");
		//mkfifo(FIFO_COMM,0777);	
	}
	fd_fifo_comm = open(FIFO_COMM,O_WRONLY);
*/
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
