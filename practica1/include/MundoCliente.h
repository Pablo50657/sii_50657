// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "Socket.h"
#include "DatosMemCompartida.h"
#define IPDIR "127.0.0.1"
#define FIFO_DIR "/tmp/myfifo"
#define FIFO_DATA "/tmp/fifodata"
#define FIFO_COMM "/tmp/fifocomm"
#define FILE_DIR "/tmp/myfile" //archivo para proyeccion en memoria

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	//bot
	int fd_mem;
	DatosMemCompartida Mem;
	DatosMemCompartida *pMem;

	//socket
	Socket s_server;
	char buffer_send[200];
	char buffer_recv[200];

	/*//servidor
	int fd_fifo_data;
	
	int fd_fifo_comm;
	char buffer_comm[5];*/


	
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
